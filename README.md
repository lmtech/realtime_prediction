# Real Time Object Detection with Alert
Predicts objects coming across mobile camera using Clarifai General Model API

There is an android app called IP Webcam that has been installed on mobile phone. We have to start the server of IP Webcam which which capture the things coming across the camera and sent it to the application.
We need Clarifai Account to use the API for object Detection. The script is written to detect the objects coming across mobile camera using Django Web Framework. 
Frontend of the application is as shown below:

![Scheme](frontend_screenshot.png)

When a person comes in front of the camera, the system will generate alert by sending a text message to the number specified in python script. The screenshot of text message is as shown below:

![Scheme](mobile_screenshot.png)

